//
//  ACGameScene.m
//  Adventure Clicker
//
//  Created by Ryan Salton on 02/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "ACGameScene.h"

@interface ACGameScene ()

@property (nonatomic, strong) SKSpriteNode *playButton;

@end

@implementation ACGameScene

- (void)didMoveToView:(SKView *)view
{
    self.backgroundColor = [UIColor greenColor];
    
    // Game Logo
    SKSpriteNode *logo = [SKSpriteNode spriteNodeWithColor:[UIColor yellowColor] size:CGSizeMake(300, 250)];
    logo.position = CGPointMake(self.size.width * 0.5, self.size.height - (logo.size.height * 0.5) - 80);
    [self addChild:logo];
    
    // Play Button
    self.playButton = [SKSpriteNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(100, 100)];
    self.playButton.position = CGPointMake(self.size.width * 0.5, logo.position.y - (logo.size.height * 0.5) - (self.playButton.size.height * 0.5) - 50);
    [self addChild:self.playButton];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

@end
